# Odoo | Deployment of Odoo via GitLab CI

[Please find the specifications by clicking](https://github.com/eazytraining/projet-fils-rouge.git)

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
The company IC GROUP, where you work as a DevOps engineer, wants to set up a showcase website to provide access to its 02 flagship applications:

Odoo and
pgAdmin

Odoo is a multi-purpose ERP that allows managing sales, purchases, accounting, inventory, personnel, etc.

Odoo is distributed in both community and Enterprise versions. ICGROUP wants to have control over the code and make its own modifications and customizations, so it has opted for the community edition. Several versions of Odoo are available, and the one chosen is 13.0 because it integrates a Learning Management System (LMS) that will be used to publish internal training and facilitate information dissemination.

Useful links:

Official website: https://www.odoo.com/
Official GitHub: https://github.com/odoo/odoo.git
Official Docker Hub: https://hub.docker.com/_/odoo

pgAdmin, on the other hand, will be used to graphically administer the previously created PostgreSQL database.

Official website: https://www.pgadmin.org/
Official Docker Hub: https://hub.docker.com/r/dpage/pgadmin4/

The showcase website was designed by the company's development team, and the related files are located in the following repository: https://github.com/eazytraining/projet-fils-rouge.git. It is your responsibility to containerize this application while allowing the input of the various application URLs (Odoo and pgAdmin) through environment variables.

Below is an overview of the expected showcase website.

>![alt text](img/image.png)
The created image should allow launching a container to host this website and have the appropriate links to access our internal applications.


## Specification : Containerization of the web application.

Indeed, this is a Python web application using the Flask module. The steps to containerize this application are as follows:

1. Base image: python:3.6-alpine
2. Set the working directory to /opt
3. Install the Flask module using pip install
4. Expose port 8080, which is the default port used by the application
5. Create environment variables ODOO_URL and PGADMIN_URL to allow the definition of these URLs when launching the container
7.  Launch the app.py application in the ENTRYPOINT using the python command

Once the Dockerfile is created, build it and launch a test container to visit the official websites of each of these applications (official websites provided above).

Image name: ic-webapp;
tag: 1.0
test container name: test-ic-webapp

Once the test is complete, remove this test container and push your image to your Docker Hub registry.


## 1. Definition of the Dockerfile
This Dockerfile is defined to package a Python-based web application, using the base image version `3.6-alpine`.
>![alt text](img/image-1.png)
*Contenu du Dockerfile*


## 2. Definition of the .gitlab-ci.yml file
### 2.1. Definition of variables
1. Server information
The test server is an EC2 instance, and the variables are explicit because it's an ephemeral server. In the case of a permanent server, create an environment variable.
>![alt text](img/image-2.png)

2. Other variables
`DOCKERHUB_PASSWORD` and `DOCKERHUB_USERNAME`: are the login credentials for Dockerhub account to allow push operations when transferring the Docker image to Dockerhub.
`ID_RSA`: contains the private SSH key used for authentication with EC2 instances used for testing environment deployment.
`IMAGE_NAME`: holds the address of the Gitlab container registry associated with this project (value: registry.gitlab.com/carlinfongang-labs/projet-odoo/odoo-gitlab-ci).
`IMAGE_TAG`: contains the default tag value used for pushing the image to Dockerhub (value: latest).

>![alt text](img/image-3.png)


### 2.2. Stage 1 :  build
The first step of this pipeline is the build, which will create a Docker image of the ic-webapp application using the previously defined Dockerfile.
>![alt text](img/image-4.png)
>![alt text](img/image-5.png)
*Build Console Output*

>![alt text](img/image-6.png)
*Artifact generated after the build*


### 2.3. Stage 2 : test acceptation
This phase is set up to ensure the reliability of the image produced after the build.
>![alt text](img/image-7.png)
*Job executed in the "Acceptance Test" phase*

>![alt text](img/image-8.png)
*Console output of the "Acceptance Test" stage*

### 2.4. Stage 3 : Release
Generation of the release and storage on the Gitlab container registry
>![alt text](img/image-9.png)

>![alt text](img/image-10.png)
*Console output of the "Release image" stage*


 ### 2.5. Stage 4 : Deploy test
Deployment configuration on EC2 instance
 >![alt text](img/image-11.png)
 *Task to be performed for the "Deploy test" stage*
>![alt text](img/image-12.png)
*Console output of the "Deploy test" stage*

>![alt text](img/image-13.png)
*Container currently running on the EC2 instance*

#### Result
>![alt text](img/image-14.png)
*Application ic-webapp*

>![alt text](img/image-15.png)
*Access to the official Odoo website*

>![alt text](img/image-16.png)
*Access to the pgadmin website*

### 2.6. Stage 5 : Testing the application in the test environment
This stage has been set up to ensure that the deployment performed is functional.
>![alt text](img/image-17.png)
>![alt text](img/image-18.png)
*Console output of the "Test test" stage*

### 2.7. Stage 6 : Stop test
This stage allows for the deletion of the test environment once the tests are completed. 
It is triggered manually.
>![alt text](img/image-19.png)
*Cleanup test environment job*

>![alt text](img/image-20.png)
*Console output of the "Stop test" stage*

### 2.8. Stage 7 : Backing up the release on Dockerhub
>![alt text](img/image-21.png)
*Release transfer job to Dockerhub*

>![alt text](img/image-22.png)
*Console output of the "Dockerhub-release" stage*

#### Result
>![alt text](img/image-23.png)
*Images pushed to Dockerhub*





