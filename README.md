# CI/CD | Mise en œuvre d'un Pipeline CI/CD avec GitLab CI pour uen application Dockerisée

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte : IC GROUP et son projet de site vitrine

Une entreprise nommée IC GROUP, souhaite mettre en place un site vitrine pour fournir un accès à ses deux applications phares :

* **Odoo** et 
* **pgAdmin**

**Odoo** est un ERP polyvalent qui permet de gérer les ventes, les achats, la comptabilité, les stocks, le personnel, etc.

Odoo est distribué en versions communautaires et entreprise. Souhaitant un contrôle accru sur le code et la possibilité de réaliser des modifications et personnalisations, IC GROUP a opté pour l'édition communautaire. Plusieurs versions d'Odoo sont disponibles, et le choix s'est porté sur la version 13.0 car elle intègre un Learning Management System (LMS) qui servira à la publication de formations internes et à la diffusion de l'information.

Liens utiles :

Site officiel : [https://www.odoo.com/](https://www.odoo.com/)
GitHub officiel : [https://github.com/odoo/odoo.git](https://github.com/odoo/odoo.git)
Docker Hub officiel : [https://hub.docker.com/_/odoo](https://hub.docker.com/_/odoo)

**pgAdmin**, quant à lui, permettra l'administration graphique de la base de données PostgreSQL créée précédemment.

Site officiel : [https://www.pgadmin.org/](https://www.pgadmin.org/)
Docker Hub officiel : [https://hub.docker.com/r/dpage/pgadmin4/](https://hub.docker.com/r/dpage/pgadmin4/)

Le site vitrine a été conçu par l'équipe de développement de l'entreprise, et les fichiers associés se trouvent dans le dépôt suivant : [Dépot site vitrine](https://github.com/eazytraining/projet-fils-rouge.git). 

Notre mission consiste à conteneuriser cette application tout en permettant la configuration des URLs des applications (Odoo et pgAdmin) via des variables d'environnement.


## Traduction du processus de conteneurisation d'une application web Flask

Il s'agit ici de deployer une application web Python utilisant le module Flask. Qeulques étapes pour conteneuriser cette application :

1. **Image de base:** `python:3.6-alpine`
2. **Répertoire de travail:** Définir le répertoire de travail sur `/opt`
3. **Installation de Flask:** Installer le module Flask avec `pip install flask`
4. **Port d'exposition:** Exposer le port 8080, qui est le port par défaut utilisé par l'application
5. **Variables d'environnement:** Créer des variables d'environnement `ODOO_URL` et `PGADMIN_URL` pour permettre la définition de ces URLs lors du lancement du conteneur.
6. **Point d'entrée:** Lancer l'application `app.py` dans l'instruction `ENTRYPOINT` en utilisant la commande `python`.

Une fois le `Dockerfile` créé, construisez-le et lancez un conteneur de test pour visiter les sites web officiels de chacune de ces applications (consultez les sites web officiels mentionnés précédemment).

Nom de l'image : `ic-webapp`
tag : `1.0`
Nom du conteneur de test : `test-ic-webapp`


## Prérequis 
 - Il est nécessaire de disposer d'une instance ou d'un serveur avec une adresse IP publique. 
    [Provisionner une instance ec2 avec terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/terraform-provide-ec2-instance.git)

 - Modifiez les règles entrantes et sortantes du groupe de sécurité associé à l'instance pour autoriser le trafic TCP. Cette configuration est requise pour nos requêtes curl ainsi que pour les protocoles HTTP et HTTPS, qui facilitent l'accès aux services web. Il est également essentiel d'autoriser le protocole SSH afin de permettre la connexion à l'hôte pendant les opérations de déploiement de l'application.


## **Configuration de Base et Services**
L'ensembe de ce travail sera organisé dans un repertoire `IC-WEBAPP` dans lequel l'on va crée le fichier `.gitlab-ci.yml` qui est le fichier reconnu par gitlab et permettant l'exécution de pipeline gitlab-ci.

Notre pipeline utilise Docker comme environnement de conteneurisation principal, avec la dernière version de l'image Docker. Nous utilisons également le service Docker-in-Docker (`docker:dind`), aliasé ici `webapp-odoo`, qui nous permet de construire et gérer des conteneurs à l'intérieur d'un conteneur GitLab CI.

```yaml
image: docker:latest
services:
  - name: docker:dind
    alias: webapp-odoo
```

### **Étapes du Pipeline**

#### **1. Déclaration des stages**

```yaml
stages:
  - Build
  - Test acceptation
  - Release 
  - Deploy test
  - Test test
  - Stop test
```

#### **2. Déclaration des varibles d'environnement**

## Traduction des variables de configuration Docker :

| Variable d'origine | Rôle | Description |
|---|---|---|
| `DOCKERHUB_PASSWORD` | Mot de passe DockerHub | Identifiant de connexion pour votre compte DockerHub. Permet de pousser des images Docker vers DockerHub. |
| `DOCKERHUB_USERNAME` | Nom d'utilisateur DockerHub | Identifiant de connexion pour votre compte DockerHub. Permet de pousser des images Docker vers DockerHub. |
| `ID_RSA` | Clé SSH privée | Contient la clé SSH privée utilisée pour l'authentification avec les instances EC2 lors du déploiement de l'environnement de test. |
| `CI_REGISTRY_IMAGE` | Gitlab Container Registry | Stocke l'adresse du registre de conteneurs GitLab associé à ce projet (valeur : `registry.gitlab.com/carlinfongang-labs/projet-odoo/ic-webapp`). |
| `IMAGE_TAG` | Étiquette de l'image | Contient la valeur d'étiquette par défaut utilisée pour pousser l'image vers DockerHub et Gilab Container Registry (valeur : `latest`). |

![alt text](img/image-28.png)

#### **3. Construction de l'Image**

Dans cette étape, nous construisons l'image Docker de notre application en utilisant le fichier Dockerfile présent dans le répertoire du projet. L'image est ensuite sauvegardée sous forme de fichier tar pour être réutilisée dans les étapes ultérieures.

```yaml
docker-build:
  stage: Build
  script:
    - docker build --tag ic-webapp:1.0 .
    - docker save ic-webapp > ic-webapp.tar
  artifacts:
    paths:
      - ic-webapp.tar
```

#### **4. Test d'Acceptation**

Après la construction, l'image est chargée et un conteneur est lancé pour vérifier son fonctionnement. Nous utilisons `curl` pour tester la réponse HTTP de l'application.

```yaml
test-acceptation:
  stage: Test acceptation
  script:
    - docker load < ic-webapp.tar
    - docker run -d -p 8080:8080 --name ic-webapp-test ic-webapp:1.0
    - sleep 5
    - curl -I "http://webapp-odoo:8080"
    - docker stop ic-webapp-test && docker rm -vf ic-webapp-test
```

#### **5. Publication sur DockerHub et GitLab Container Registry**

Les images construites sont taguées et poussées respectivement sur DockerHub et GitLab Container Registry pour garantir leur disponibilité pour des déploiements futurs et une redondance des release en cas d'incident d'une des sauvegardes.

```yaml
dockerhub-release:
  stage: Release
  script: 
    - docker load < ic-webapp.tar
    - docker tag ic-webapp:1.0 "${DOCKERHUB_USERNAME}/${REPO_NAME}:${CI_COMMIT_REF_NAME}"
    - docker tag ic-webapp:1.0 "${DOCKERHUB_USERNAME}/${REPO_NAME}:${CI_COMMIT_SHORT_SHA}"
    - echo "$DOCKERHUB_PASSWORD" | docker login -u "$DOCKERHUB_USERNAME" --password-stdin
    - docker push "${DOCKERHUB_USERNAME}/${REPO_NAME}:${CI_COMMIT_REF_NAME}"
    - docker push "${DOCKERHUB_USERNAME}/${REPO_NAME}:${CI_COMMIT_SHORT_SHA}"

gitlab-release:
  stage: Release
  script:
    - docker load < ic-webapp.tar
    - docker tag ic-webapp:1.0 "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}"
    - docker tag ic-webapp:1.0 "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
    - docker push "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
```

![alt text](img/image-23.png)
*Release Dockerhub*

#### **6. Déploiement de Test**
Nous déployons l'application dans un environnement de test en utilisant SSH pour nous connecter et Docker sur un serveur distant. Ceci est crucial pour voir comment l'application se comporte dans un environnement qui imite la production.

```yaml
deploy-test: #
  image: alpine:latest
  stage: Deploy test
  script:
    - docker stop test-${REPO_NAME} && docker rm -vf test-${REPO_NAME} || true
    - chmod 600 $ID_RSA
    - apk update && apk add openssh-client
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "echo $DOCKERHUB_PASSWORD | docker login -u $DOCKERHUB_USERNAME --password-stdin"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "sudo docker pull ${DOCKERHUB_USERNAME}/${REPO_NAME}:${CI_COMMIT_REF_NAME}"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "sudo docker container rm -f test-${REPO_NAME} || true"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "sudo docker run --rm -d -p 8080:8080 --name test-${REPO_NAME} ${DOCKERHUB_USERNAME}/${REPO_NAME}:${CI_COMMIT_REF_NAME}"
  environment:
    name: test
    url: http://$SERVER_IP:8080
  only:
    - main
```
![alt text](img/image-24.png)
*Sortie console du deploiement en environnement de test*

#### **7. Tests dans l'Environnement de Test**
Des tests supplémentaires sont exécutés dans l'environnement de test pour s'assurer que tout fonctionne comme prévu.

```yaml
test-test:
  <<: *test
  stage: Test test
  variables:
    DOMAIN: $SERVER_IP:8080
```
le template de test que nous avons définit au tout début du pipeline, nous permet ici d'exécuter une requette curl sur l'ip de l'hôte:port afin de nous assurer qu'il y'a bien une reponse 200 en retour.

![alt text](img/image-25.png)
*L'application est disponible en environnement de test*

Elle est également accéssible via le navigateur

![alt text](img/image-26.png)

#### **8. Arrêt et Nettoyage**
Finalement, le conteneur de test est proprement arrêté et supprimé pour libérer les ressources.

```yaml
stop-test:
  stage: Stop test
  variables:
    GIT_STRATEGY: none
  when: manual
  environment:
    name: test/$CI_COMMIT_REF_NAME
    action: stop
  script:
    - chmod 600 $ID_RSA
    - apk update && apk add openssh-client
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker container rm -f ${DOCKERHUB_USERNAME}${REPO_NAME} || true"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker stop test-${REPO_NAME} && docker rm -vf test-${REPO_NAME}"
```
Le job **"stop-test"** est configuré pour être exécuté manuellement. Cela offre aux développeurs la flexibilité de contrôler quand arrêter l'environnement de test, permettant potentiellement un accès prolongé pour des investigations ou des démonstrations après les tests automatisés.

#### **9. Vue d'ensemble du pipeline**
![alt text](img/image-27.png)

### Conclusion
Ce pipeline démontre un flux de travail complet de l'intégration et la livraison continues pour une application Dockerisée. Chaque étape est conçue pour garantir que l'application est construite, testée et déployée de manière efficace et sécurisée, réduisant ainsi les erreurs et améliorant la qualité du produit final.
